#include "CMSPhase2Event2StdEventConverter.hh"

using namespace eudaq;

namespace {
  auto dummy0 = eudaq::Factory<eudaq::StdEventConverter>::
  Register<CMSPhase2RawEvent2StdEventConverter>(CMSPhase2RawEvent2StdEventConverter::m_id_factory);
}

bool CMSPhase2RawEvent2StdEventConverter::Converting(eudaq::EventSPC pEvent, eudaq::StandardEventSP pStdEvent, eudaq::ConfigurationSPC conf) const
{

  // No event
  if(!pEvent || pEvent->GetNumSubEvent() < 1) {
    return false;
  }
  //Create one StandardPlane for each block of data
  auto cSubEventRaw = std::dynamic_pointer_cast<const eudaq::RawEvent>(pEvent->GetSubEvent(0));
  
  std::vector<eudaq::StandardPlane*> cPlanes;  
  uint32_t  cNFrames = pEvent->GetSubEvents().size();
  for(uint32_t cBlockId = 0; cBlockId<cSubEventRaw->GetBlockNumList().size(); cBlockId++){
    eudaq::StandardPlane *cPlane = new eudaq::StandardPlane(cBlockId+30, "CMSPhase2StdEvent", "CMSPhase2" ); 
    cPlane->SetSizeZS(1016, 2, 0, cNFrames); // 3 values per hit, 2 uint8t words per uint16t word, 1 for header
    cPlanes.push_back(cPlane);
  }

  // Add additional plane for stub seed position
  /*eudaq::StandardPlane *corrPlane = new eudaq::StandardPlane(40, "CMSPhase2StdEvent", "CMSPhase2" ); 
  corrPlane->SetSizeZS(1016, 2, 0, cNFrames); // 3 values per hit, 2 uint8t words per uint16t word, 1 for header
  cPlanes.push_back(corrPlane);*/
  eudaq::StandardPlane *seedPlane = new eudaq::StandardPlane(41, "CMSPhase2StdEvent", "CMSPhase2" ); 
  seedPlane->SetSizeZS(1016, 2, 0, cNFrames); // 3 values per hit, 2 uint8t words per uint16t word, 1 for header
  cPlanes.push_back(seedPlane);


  for(uint32_t cFrameId=0; cFrameId<cNFrames ; cFrameId++){
    cSubEventRaw = std::dynamic_pointer_cast<const eudaq::RawEvent>(pEvent->GetSubEvent(cFrameId)); 
    for(uint32_t cBlockId=0; cBlockId < cSubEventRaw->GetBlockNumList().size(); cBlockId++){
      AddFrameToPlane(cPlanes.at(cBlockId), cSubEventRaw->GetBlock(cBlockId), cFrameId, cNFrames ); 
    }
    // Add stub information from tags
    std::map<std::string, std::string> cRawTags = cSubEventRaw.get()->GetTags();
    
    // for (int i = 0; i < cRawTags.size(); i++)
    for (auto item : cRawTags) {
      if (item.first.find("stub_pos_") != std::string::npos) {
        std::vector<std::string> results;
        boost::split(results, item.first, [](char c){return c == '_';});
        int cHybridId = std::stoi(results[2]);
        int cCbcId = std::stoi(results[3]);
        int cStubId = std::stoi(results[4]);
        int cStubPosition = std::stoi(item.second);
        char name[100];
        std::sprintf(name, "stub_bend_%02d_%02d_%02d", cHybridId, cCbcId, cStubId);
        int cStubBend = std::stoi(cRawTags[name]);
        std::cout << "Found stub on FE" << cHybridId << ", CBC " << cCbcId << " at position " << cStubPosition << " with bend " << cStubBend << std::endl;
        int cGlobalSeed = cCbcId*254 + cStubPosition;
        int cGlobalCorr = cGlobalSeed + 2*convertBendCode(cStubBend);
        std::cout << "Converted to global coordinates " << 1-cHybridId << "\t" << cGlobalSeed << " and " << cGlobalCorr << std::endl;
        seedPlane->PushPixel(cGlobalSeed, 1-cHybridId, 1, false, 1);
        // corrPlane->PushPixel(cGlobalCorr, 1-cHybridId, 1, false, 1);
      }
    }

  }
  for(auto cPlane : cPlanes ){
    pStdEvent->AddPlane(*cPlane);
  } 
  return true;
}

void CMSPhase2RawEvent2StdEventConverter::AddFrameToPlane(eudaq::StandardPlane *pPlane, const std::vector<uint8_t> &data, uint32_t pFrame, uint32_t pNFrames) const{

  // get width and height
  uint32_t width = (((uint32_t)data[1]) << 8) | (uint32_t)data[0];
  uint32_t height = (((uint32_t)data[3]) << 8) | (uint32_t)data[2];

  // set size
  uint32_t nhits = data.size()/6 - 1;

  // process data
  for(size_t i = 0; i < nhits; i++){    
    // column, row, tot, ?, lvl1
    pPlane->PushPixel(getHitVal(data, i, 0), getHitVal(data, i, 1), getHitVal(data, i, 2), false, pFrame);  
  }
}

uint16_t CMSPhase2RawEvent2StdEventConverter::getHitVal(const std::vector<uint8_t>& data, size_t hit_id, size_t value_id) const
{
  uint32_t word_index = 6 + hit_id*6 + value_id*2;
  return (((uint16_t)data[word_index + 1]) << 8) | (uint16_t)data[word_index];
}

int CMSPhase2RawEvent2StdEventConverter::convertBendCode(int pBend) const {
    // BendCode | Bends
    // -------------------
    // 0xF = 15 | -6.5, -7
    // 0xE = 14 | -5.5, -6
    // 0xD = 13 | -4.5, -5
    // 0xC = 12 | -3.5, -4
    // 0xB = 11 | -2.5, -3
    // 0xA = 10 | -1.5, -2
    // 0x9 =  9 | -0.5, -1
    // 0x8 =  8 | INVALID CODE
    // 0x7 =  7 | 6.5, 7
    // 0x6 =  6 | 5.5, 6
    // 0x4 =  5 | 4.5, 5
    // 0x3 =  4 | 3.5, 4
    // 0x5 =  3 | 2.5, 3
    // 0x2 =  2 | 1.5, 2
    // 0x1 =  1 | 0.5, 1
    // 0x0 =  0 | 0
    // if (pBend == 4) return 5;
    //if (pBend == 3) return 4;
    // if (pBend == 5) return 3;
    if (pBend > 8) return -pBend+8;
    if (pBend < 8) return pBend;
    return -100; 
}
